# Gitter iOS App

Room list | Chat view
--- | ---
![](https://i.imgur.com/FmWOkTx.png) | ![](https://i.imgur.com/90RSAkn.png)


## How to start

 1. Clone and setup the [`webapp`](https://gitlab.com/gitlab-org/gitter/webapp) project
    - At a very minimum, `npm install`. You don't necessarily need to run the webapp locally and can skip the Docker stuff
 1. In the [`webapp`](https://gitlab.com/gitlab-org/gitter/webapp) project, run `npm run build-ios-assets`
 1. Symlink the webapp embedded build asset output to the Android project
    - macOS: `ln -s /Users/<YOUR_USERNAME>/Documents/gitlab/webapp/output/ios/www /Users/<YOUR_USERNAME>/Documents/gitlab/gitter-ios-app/Gitter/www`
 1. Make a copy of `Gitter/GitterSecrets-Dev.plist.example` named `Gitter/GitterSecrets-Dev.plist` and follow the comment instructions inside
    - You may also need to manually add this to the bundle. In the Xcode navigator tab on the left, open up the `Gitter` folder, right-click on the `Supporting Files` folder, `Add files to "Gitter"...`, and select the secret plist file, https://i.imgur.com/GGJ1Uu3.png
 1. Update submodules after clone: `git submodule update --init --recursive`
 1. Open in Xcode: `open Gitter.xcworkspace`


## Run against localhost

In `Edit Scheme...` > `Arguments` > `Arguments Passed On Launch` add the following:

 1. `-TroupeURL`
 1. `http://localhost:5000`
 1. `-TroupeWSURL`
 1. `ws://localhost:5000/faye`
 1. If you need to sign in, update your OAuth secrets in `Gitter/GitterSecrets-Dev.plist` with something from your local instance



## Developer and debugging notes

If you are running into any issues, try `Product -> Clean` in Xcode


### Find error/exception causing `SIGABRT` crashing app

See https://mukeshthawani.com/debug-the-sigabrt-error-exception


### `pod install` fails with LibComponentLogging

If [aharren/LibComponentLogging-Core#24](https://github.com/aharren/LibComponentLogging-Core/issues/24) is still an issue, follow the instructions in [this comment](https://github.com/aharren/LibComponentLogging-Core/issues/24#issuecomment-44310097).

Originally forked from https://bitbucket.org/troupe/troupeappios


### Generic App Store update messages

English (U.K.)
> We are always trying to improve the Gitter app with each release. If you enjoy our improvements, let us know by reviewing our app.

Chinese (Simplified)
> 我们一直在努力改善与每个版本的应用程序。如果你喜欢我们的改进，请让我们通过回顾我们的应用程序知道。

French
> Nous nous efforçons d'améliorer l'application Gitter, en développant régulièrement de nouvelles versions. Si vous en appréciez les nouvelles fonctionnalités, nous vous invitons à nous le faire savoir nous en laissant vos commentaires!

Japanese
> Gitterアプリは、各リリースごとの改良に努めています。改善点が気に入りましたらアプリのレビュー投稿をお願いします。

Portuguese (Brazil)
> Estamos sempre trabalhando para melhorar o Gitter. Se você curtiu nossas atualizações, que tal escrever um review?

Russian
> С каждый новой версией приложения Gitter мы стараемся улучшить его работу. Если вам нравятся произошедшие изменения, пожалуйста, оцените наше приложение.
