import Foundation

@objc class BetaCleaner: NSObject {

    // The old beta test app would create keychain items with the same kSecAttrGeneric as the prod app.
    // This would be skipped over by GTMOAuth2, but picked up by the keychain wrapper, causing a clash.
    // This function removes the old beta keychain items so that only prod can be found.
    static func clean() {
        let query = NSMutableDictionary()
        query.setValue(kSecClassGenericPassword, forKey: kSecClass as String)
        query.setValue("OAuth", forKey: kSecAttrGeneric as String)
        query.setValue("Gitter: https://beta.gitter.im", forKey: kSecAttrService as String)
        query.setValue("OAuth", forKey: kSecAttrAccount as String)

        let status: OSStatus = SecItemDelete(query);

        if status == noErr {
            print("removed beta access token keychain item")
        }
    }
}