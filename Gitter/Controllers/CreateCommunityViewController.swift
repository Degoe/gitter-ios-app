import UIKit
import WebKit

class CreateCommunityViewController: UIViewController, WKNavigationDelegate, UINavigationBarDelegate {

    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    var delegate: CreateCommunityViewControllerDelegate?
    private var webView: WKWebView?
    private let loginData = LoginData()
    private let rootURL = URL(string: "https://gitter.im")!
    private let roomThatWillNeverBeDeleted = "/gitterHQ/gitter"

    private var loading = true {
        didSet {
            if (loading) {
                loadingIndicator.startAnimating()
            } else {
                loadingIndicator.stopAnimating()
            }
            webView?.isHidden = loading
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.delegate = self

        ClearCookies {
            let webView = WKWebView()
            self.loading = true

            webView.scrollView.bounces = false
            webView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(webView)
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webView]|", options: [], metrics: nil, views: ["webView": webView]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[navigationBar][webView]|", options: [], metrics: nil, views: ["navigationBar": self.navigationBar, "webView": webView]))
            webView.navigationDelegate = self
            webView.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
            self.webView = webView

            var url = URLComponents(url: self.rootURL, resolvingAgainstBaseURL: false)!
            // this modal is only available from a room
            url.path = self.roomThatWillNeverBeDeleted
            url.fragment = "createcommunity"

            let request = NSMutableURLRequest(url: url.url!)
            request.addValue("Bearer \(self.loginData.getAccessToken()!)", forHTTPHeaderField: "Authorization")
            if (SecretMenuData().useStagingApi) {
                request.addValue("gitter_staging=staged;", forHTTPHeaderField: "Cookie")
            }

            webView.load(request as URLRequest)
        }
    }

    @IBAction func onCancelTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("var style = document.createElement('style');" +
            "style.type = 'text/css';" +
            "style.innerHTML = '.community-create-close-button { display: none; }';" +
            "document.getElementsByTagName('head')[0].appendChild(style);", completionHandler: nil)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url!

        guard url.host == rootURL.host else {
            return decisionHandler(.cancel)
        }

        if !url.path.hasPrefix(roomThatWillNeverBeDeleted) {
            let roomName = String(url.path.characters.dropFirst())
            decisionHandler(.cancel)
            dismiss(animated: true, completion: {
                self.delegate?.didCreateRoom(withName: roomName)
            })
        } else {
            decisionHandler(.allow)
        }
    }

    deinit {
        webView?.removeObserver(self, forKeyPath: "loading")
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "loading") {
            loading = webView!.isLoading
        }
    }
}

protocol CreateCommunityViewControllerDelegate {
    func didCreateRoom(withName name: String)
}
