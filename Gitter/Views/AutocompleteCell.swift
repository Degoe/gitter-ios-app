import Foundation

class AutocompleteCell: UITableViewCell {
    @IBOutlet var avatarImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var avatarImageHeight: NSLayoutConstraint!

    var avatarImageHeightInPixels: Int {
        get {
            let points = avatarImageHeight.constant
            let scale = UIScreen.main.scale
            return Int(points * scale)
        }
    }
}
