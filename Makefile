SHELL=/bin/bash

OUTPUT := builds/output

TYPE := beta

ifeq '$(TYPE)' 'beta'
PRODUCT := GitterBeta
APP_NAME := GitterBeta.app
CODESIGN_IDENTITY := iPhone Distribution: Troupe Technology Limited (A86QBWJ43W)
TEST_FLIGHT_NOTIFY = false
else
PRODUCT := GitterBeta
APP_NAME := GitterBeta.app
CODESIGN_IDENTITY := iPhone Distribution: Troupe Technology Limited (A86QBWJ43W)
TEST_FLIGHT_NOTIFY = false
endif

LATEST_ARCHIVE = $(shell find ~/Library/Developer/Xcode/Archives -name '$(PRODUCT) *.xcarchive'|tail -1)

LATEST_APP = $(shell find "$(LATEST_ARCHIVE)" -name "$(APP_NAME)" -print)
LATEST_DSYM = $(shell find "$(LATEST_ARCHIVE)" -name "$(APP_NAME).dSYM" -print)

# These come from Gitter secrets repo, https://gitlab.com/gl-gitter/secrets (see /ios/prod or /ios/prod-dev)
TEST_FLIGHT_API_TOKEN = $(TEST_FLIGHT_API_TOKEN)
TEST_FLIGHT_TEAM_TOKEN = $(TEST_FLIGHT_TEAM_TOKEN)

BUILD_URL = $$BUILD_URL

all: compile

ci: clean compile

clean:
	rm -rf $(OUTPUT)/*

compile: output-location
	./xctool/xctool.sh \
		-workspace Gitter.xcworkspace \
		-scheme "$(PRODUCT)" \
		archive

fetch-embedded-webapp:
	rm -rf Troupe/www/build/*
	cp -R ../gitter-webapp/output/embedded/* Troupe/www/build/

output-location:
	mkdir -p $(OUTPUT)

change-log: output-location
ifndef BUILD_URL
		curl $(BUILD_URL)/api/xml?xpath=/freeStyleBuild/changeSet > $(OUTPUT)/changes.xml
		xsltproc scripts/changes.xslt $(OUTPUT)/changes.xml > $(OUTPUT)/changes.txt
endif

$(LATEST_ARCHIVE): compile
$(LATEST_APP): $(LATEST_ARCHIVE)
$(LATEST_DSYM): $(LATEST_ARCHIVE)

ipa: compile
	/usr/bin/xcrun -sdk iphoneos PackageApplication \
		-v "$(LATEST_APP)" \
		-o "$(realpath $(OUTPUT))/$(PRODUCT).ipa" \
		--sign "$(CODESIGN_IDENTITY)" \
		--embed provisioning/GitterBetaAdhoc.mobileprovision

zip-dSYM: compile output-location
	rm -f $(OUTPUT)/$(PRODUCT)-dSYM.zip
	zip -r $(OUTPUT)/$(PRODUCT)-dSYM.zip "$(LATEST_DSYM)"

upload: change-log ipa zip-dSYM
ifndef BUILD_URL
	curl "http://testflightapp.com/api/builds.json" \
	  -F file=@'$(OUTPUT)/$(PRODUCT).ipa' \
	  -F dsym=@"$(OUTPUT)/$(PRODUCT)-dSYM.zip" \
	  -F api_token="$(TEST_FLIGHT_API_TOKEN)" \
	  -F team_token="$(TEST_FLIGHT_TEAM_TOKEN)" \
	  -F distribution_lists="TroupeDistribution" \
	  -F notify="$(TEST_FLIGHT_NOTIFY)" \
	  -F notes=@'$(OUTPUT)/changes.txt'
else
	curl "http://testflightapp.com/api/builds.json" \
	  -F file=@'$(OUTPUT)/$(PRODUCT).ipa' \
	  -F dsym=@"$(OUTPUT)/$(PRODUCT)-dSYM.zip" \
	  -F api_token="$(TEST_FLIGHT_API_TOKEN)" \
	  -F team_token="$(TEST_FLIGHT_TEAM_TOKEN)" \
	  -F distribution_lists="TroupeDistribution" \
	  -F notify="$(TEST_FLIGHT_NOTIFY)" \
	  -F notes="automated build"
endif
